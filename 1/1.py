import pickle

with open('../prime_dict.pickle', 'rb') as handle:
    tmp_prime_dict = pickle.load(handle)
prime_dict = {3: 7}
for i, val in tmp_prime_dict.items():
    prime_dict[i - 1] = val


def binary_encode(value, length):
    """
    :param length: int32
    :param value: int32/64
    :returns binary_value: String (binary format)
    """
    assert (len(bin(value)) - 2 <= length)
    return "0" * (length - len(bin(value)) + 2) + bin(value)[2:]


def binary_decode(binary_value):
    return int(binary_value, 2)


def mod_exp(g, x, p):
    """ g^x mod p"""
    res = 1
    g = g % p
    if g == 0:
        return 0

    while x > 0:
        res = (res * g) % p if x & 1 == 1 else res
        x = x >> 1
        g = (g * g) % p

    return res


def msb(x):
    return x[0]


class PRG:
    """ Pseudo-Random Generator """

    def __init__(self, seed=None, expansion_factor=32):
        """
        Initialize PRG with given/ default values
        :param seed: String s ∈ {0,1}^n
        :param expansion_factor: int
            - L(n) {> n}
        """
        self.__seed = seed if seed is not None else binary_encode(42, 32)
        self.__seed_len = len(self.__seed)
        self.__expansion_factor = expansion_factor if expansion_factor is not None else 64

        # Confirm if this assert makes sense or not
        # i.e. Is a group with 2 bits or less viable for good keys?
        assert (self.__seed_len >= 3)
        self.__G = prime_dict[
            self.__seed_len - 1] if self.__seed_len <= 64 else int(
            1e9 + 7)  # Prime number not included in the prime_dict
        self.__p = prime_dict[self.__seed_len] if self.__seed_len <= 64 else \
        prime_dict[64]

    def generate(self):
        """ Generate the key from the seed """
        if self.__seed_len >= self.__expansion_factor:
            raise Exception(
                f"L(n) should be greater than n, where n is the seed and L is the output length!")

        # print(f"g: {self.__G}\t p: {self.__p}")
        result = ''
        seed = self.__seed
        for i in range(self.__expansion_factor):
            # print(f"{i} iter:\t bit is: {msb(seed)} \t seed is {seed}")
            result += msb(seed)
            seed = binary_encode(
                mod_exp(self.__G, binary_decode(seed), self.__p),
                self.__seed_len)
        return result


if __name__ == "__main__":
    # Here, choosing a seed s \in {0,1}^10, s=42
    seed = binary_encode(42, 10)
    # Repeatedly using same seed values with same expansion factors gives same values here
    for _ in range(5):
        GENERATOR = PRG(seed=seed, expansion_factor=12)
        print("Same values of seed and expansion factor...")
        val = GENERATOR.generate()
        print(val)
    print("Different values of seeds should generate different values")
    seeds = [binary_encode(x, 10) for x in [5, 12, 32, 49, 400, 950]]
    for seed in seeds:
        GENERATOR = PRG(seed=seed, expansion_factor=12)
        print(
            f"Seed value: {seed}\t Generated Pseudorandom value: {GENERATOR.generate()}")
