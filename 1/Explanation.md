# Pseudo-Random Generator

<p style="text-align:right;"> <b> Aman Rojjha </b> <br> 2019111018 </p>

A pseudo-random generator $G$ is a deterministic algorithm for transforming a
uniform string (called a *seed*) into a longer, *uniform-looking*/ *
pseudorandom* output string.

## Motivation

**PRG** uses a *small* amount of true randomness to generate a large amount of *
pseudo-randomness*, which is useful when introducing large randomness in the
system is *infeasible* or too slow.

```ad-note
title: Pseudorandomn Distribution
Let $D$ be a distribution on $l$-bit strings. $D$ is *pseudorandom* if the experiment in which a string is sampled from $D$ is indistinguishable from the experiement where a uniform string of length $l$ is sampled.
More formally, it should be infeasible for any polynomial-algorithm to determine whether it is sampled from $D$ or the uniform $l$-bit string distribution.
```

## Definition

Let $G$ be a *deterministic polynomial-time algorithm* such that for any $n$ and
any input $s \in \{0, 1\}^n$, the result $G(s)$ is a string of length $l(n)$.
$G$ is a pseudo-random generator if the following conditions hold:

- **Expansion**: $\ell (n) > n\ \ \forall n$
- **Pseudorandomness**: For any **PPT** algorithm $D$, there is a negligible
  function $\text{negl}$ such that $$ \mid Pr[D(G(s)) = 1] - Pr[D(r) = 1] \le
  \text{negl}(n) $$ under some conditions.
- Note that $\ell (n)$ is called the **expansion factor** of $G$.

- The seed $s$ must be chosen uniformly and be kept secret from any adversary if
  we want $G(s)$ to look random.

### Existence of pseudo-random generators

The question whether pseudo-random generators unconditionally exists or not is
still a mystery. But under the *weaker assumption* of *one-way functions* (
thanks to the **DLP**).

## Expansion in Pseudo-Random Generator

```ad-note
title: Theorem 1
Assume that there is a pseudorandom generator with expansion factor $\ell (n) = n + 1$. Then for any polynomial $p(\cdot)$, there exists a pseudorandom generator with expansion factor $\ell (n) = p(n)$.
```

### Construction

![[fig1.png]]

1. Set $t_0 := s$. For $i=1, \ldots, p(n)$ do:
    - Let $s_{i-1}$ be the first $n$ bits of $t_{i-1}$ and let $\sigma_{i-1}$
      denote the remaining $i-1$ bits.
    - Let $t_i := G(s_{i-1}) \| \sigma_{i-1}$.
2. Output $t_{p(n)}$.

The above construction is used to provably derive a psedo-random generator
having *arbitrary expansion factor*.

## The formulation using DLP

```ad-question
title: What are **Hardcore Predicates**?
Informally, it is the computationally hardest bit of information about $x$ to obtain from $x$.
Formally, a function $\text{hc} : \{0,1 \}^* \to \{0,1\}$ is a **hard-core predicate** of a function $f$ if:
1. $\text{hc}$ can be computed in polynomial time,
2. for every probabilistic polynomial-time $\mathcal{A}$ there exists a negligible function $\text{negl}$ such that when $x$ is uniformly sampled from the distribution $\{0,1\}^*n$, $$ Pr \left[\mathcal{A}(f(x)) = \text{hc}(x)\right] \le \dfrac 1 2 + \text{negl}(n) $$ 
where the probability is taken over the uniform choice of $x$ in $\{0,1\}^n$ and the random coin tosses of $\mathcal{A}$.
```

```ad-note
title: One-Way functions to PRGs
Let $f$ be a *one-way* permutation and let **hc** be hard-core predicate of $f$. Then $$G(s) = (f(s), \text{hc}(s))$$ constitutes a pseudorandom generator with expansion factor $\ell (n) = n + 1$.
```

Particular, we choose $f$ (the *one-way permutation*) to be $g^s\text{ mod }p$
for a given group $<G, p, g>$ and $\text{hc}$ be the **most-significant bit**
$\text{msb}(x)$ for our formulation to give the pseudorandom generator $G$ with
$\ell (n) = n + 1$.