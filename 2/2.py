import pickle

with open('../prime_dict.pickle', 'rb') as handle:
    tmp_prime_dict = pickle.load(handle)
prime_dict = {3: 7}
for i, val in tmp_prime_dict.items():
    prime_dict[i - 1] = val


def binary_encode(value, length):
    """
    :param length: int32
    :param value: int32/64
    :returns binary_value: String (binary format)
    """
    assert (len(bin(value)) - 2 <= length)
    return "0" * (length - len(bin(value)) + 2) + bin(value)[2:]


def binary_decode(binary_value):
    return int(binary_value, 2)


def mod_exp(g, x, p):
    """ g^x mod p"""
    res = 1
    g = g % p
    if g == 0:
        return 0

    while x > 0:
        res = (res * g) % p if x & 1 == 1 else res
        x = x >> 1
        g = (g * g) % p

    return res


def msb(x):
    return x[0]


class PRG:
    """ Pseudo-Random Generator """

    def __init__(self, seed=None, expansion_factor=32):
        """
        Initialize PRG with given/ default values
        :param seed: String s ∈ {0,1}^n
        :param expansion_factor: int
            - L(n) {> n}
        """
        self.__seed = seed if seed is not None else binary_encode(42, 32)
        self.__seed_len = len(self.__seed)
        self.__expansion_factor = expansion_factor if expansion_factor is not None else 64
        # print(self.__seed)
        assert (self.__seed_len >= 3)
        self.__G = prime_dict[
            self.__seed_len - 1] if self.__seed_len <= 64 else int(
            1e9 + 7)  # Prime number not included in the prime_dict
        self.__p = prime_dict[self.__seed_len] if self.__seed_len <= 64 else \
            prime_dict[64]

    def generate(self):
        """ Generate the key from the seed """
        if self.__seed_len >= self.__expansion_factor:
            raise Exception(
                f"L(n) should be greater than n, where n is the seed and L is the output length!")

        # print(f"g: {self.__G}\t p: {self.__p}")
        result = ''
        seed = self.__seed
        for i in range(self.__expansion_factor):
            # print(f"{i} iter:\t bit is: {msb(seed)} \t seed is {seed}")
            result += msb(seed)
            seed = binary_encode(
                mod_exp(self.__G, binary_decode(seed), self.__p),
                self.__seed_len)
        return result


class PRF(PRG):
    """ Pseudo-Random Function """

    def __init__(self, k=None):
        """
        :param k: binary_encoded string value (seed)
        :param x: binary encoded string value (inp)
        """
        super().__init__(seed=k, expansion_factor=2 * len(k))
        self.__x = None
        self.__k = k

    def generateF(self, x=None):
        """
        F_k(x)
        :param x: binary encoded string value (input)
        :return: binary_encoded string value
        """
        assert (len(x) == len(self.__k))
        self.__x = x
        which_half = lambda inp, bit: inp[
                                      :len(inp) // 2] if bit == '0' else inp[
                                                                         len(inp) // 2:]
        seed = self.__k
        for i in self.__x:
            res = self.generate()
            seed = which_half(res, i)
        return seed


if __name__ == "__main__":
    # k = binary_encode(42, 10)
    print(f"For different value of k and same x, PRF output should be different..")
    X = [3, 49, 50, 100, 999, 500, 720]
    K = [420, 99, 100, 50, 13, 11, 190, 1000]
    X = [binary_encode(x, 10) for x in X]
    K = [binary_encode(x, 10) for x in K]
    for k in K:
        print(f"k: {k}", end="\t")
        GEN_F = PRF(k)
        value = GEN_F.generateF(X[0])
        print(f"output: {value}")


    print(f"For same value of k and different x, PRF output should be different..")
    GEN_F = PRF(K[2])
    for x in X:
        value = GEN_F.generateF(x)
        print(f"x: {x}\t output: {value}")