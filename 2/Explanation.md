# Pseudo-Random Function

<p style="text-align:right;"> <b> Aman Rojjha </b> <br> 2019111018 </p>

## Motivation

Pseudorandom functions (PRFs) generalize the notion of pseudorandom generators.

```ad-question
title: Keyed functions
In short, they are used to describe the distribution on functions.
A keyed function $F : \{0, 1\}^* \times \{0, 1\}^*$ is a two-input function, with *key* $k$ and value $x$ as the parameters. 
```

A pseudorandom function is a keyed function $F$ such that $F_k$ (for *uniform*
$k \in \{0, 1\}^n$) is indistinguishable from $f$ (for *uniform* $f \in \text{
Func}_n)$.

### Definition

An *efficient, length preserving, keyed function* $F: \{0,1\}^* \times \{0,1\}^*
\to \{0,1\}^*$ is a pseudorandom function if for all proababilistic
polynomial-time distinguishers $D$, there is negligible function $\text{negl}$
such that : $$\left| Pr[D^{F_k(\cdot)}(1^n) = 1] - Pr[D^{f(\cdot)}(1^n) = 1]
\right| \le \text{negl}(n)$$ under conditions of uniform choice of $k \in
\{0,1\}^n$, the randomness of $D$ and over the uniform choice of $f \in
\text{Func}_n$ and the randomness of $D$.

## Pseudorandom Function from PRG

Let $G$ be a pseudorandom generator with expansion factor $\ell (n) = 2 n$ and
define $G_0, G_1$ as in the text. For $k \in \{0,1\}^n$, define the function
$F_k: \{0,1\}^n \to \{0,1\}^n$ as $$F_k(x_1x_2\ldots x_n) = G_{x_n}(\ldots(G_
{x_1}(k))\ldots)$$
![[fig.png]]

Intuitively, we can acknowledge how greatly a minor change (single bit) in input
$x$ changes the path down the tree (uniformly) {we get a whole different $n$-bit
word for a single bit} and thus the final output.