import pickle

with open('../prime_dict.pickle', 'rb') as handle:
    tmp_prime_dict = pickle.load(handle)

prime_dict = {3: 7}

for i, val in tmp_prime_dict.items():
    prime_dict[i - 1] = val

def binary_encode(value, length):
    """
    :param length: int32
    :param value: int32/64
    :returns binary_value: String (binary format)
    """
    assert (len(bin(value)) - 2 <= length)
    return "0" * (length - len(bin(value)) + 2) + bin(value)[2:]


def binary_decode(binary_value):
    return int(binary_value, 2)


def mod_exp(g, x, p):
    """ g^x mod p"""
    res = 1
    g = g % p
    if g == 0:
        return 0

    while x > 0:
        res = (res * g) % p if x & 1 == 1 else res
        x = x >> 1
        g = (g * g) % p

    return res


def msb(x):
    return x[0]

class PRG:
    """ Pseudo-Random Generator """

    def __init__(self, seed=None, expansion_factor=32):
        """
        Initialize PRG with given/ default values
        :param seed: String s ∈ {0,1}^n
        :param expansion_factor: int
            - L(n) {> n}
        """
        self.__seed = seed if seed is not None else binary_encode(42, 32)
        self.__seed_len = len(self.__seed)
        self.__expansion_factor = expansion_factor if expansion_factor is not None else 64
        # print(self.__seed)
        assert (self.__seed_len >= 3)
        self.__G = prime_dict[
            self.__seed_len - 1] if self.__seed_len <= 64 else int(
            1e9 + 7)  # Prime number not included in the prime_dict
        self.__p = prime_dict[self.__seed_len] if self.__seed_len <= 64 else \
            prime_dict[64]

    def generate(self):
        """ Generate the key from the seed """
        if self.__seed_len >= self.__expansion_factor:
            raise Exception(
                f"L(n) should be greater than n, where n is the seed and L is the output length!")

        # print(f"g: {self.__G}\t p: {self.__p}")
        result = ''
        seed = self.__seed
        for i in range(self.__expansion_factor):
            # print(f"{i} iter:\t bit is: {msb(seed)} \t seed is {seed}")
            result += msb(seed)
            seed = binary_encode(
                mod_exp(self.__G, binary_decode(seed), self.__p),
                self.__seed_len)
        return result


class PRF(PRG):
    """ Pseudo-Random Function """

    def __init__(self, k=None):
        """
        :param k: binary_encoded string value (seed)
        :param x: binary encoded string value (inp)
        """
        super().__init__(seed=k, expansion_factor=2 * len(k))
        self.__x = None
        self.__k = k

    def generateF(self, x=None):
        """
        F_k(x)
        :param x: binary encoded string value (input)
        :return: binary_encoded string value
        """
        assert (len(x) == len(self.__k))
        self.__x = x
        which_half = lambda inp, bit: inp[
                                      :len(inp) // 2] if bit == '0' else inp[
                                                                         len(inp) // 2:]
        seed = self.__k
        for i in self.__x:
            res = self.generate()
            seed = which_half(res, i)
        return seed


class CPA:
    """ Chosen Plaintext Attack """

    def __init__(self):
        self.__n = None

    def gen(self, n=None):
        """
        Key Generator
        :param n: int32/64
            - Number of bits in the key
        :returns: binary_encoded random n-bit value
        """
        from random import getrandbits
        return binary_encode(getrandbits(n),
                             n) if n is not None else binary_encode(
            getrandbits(self.__n), self.__n)

    def enc(self, msg, key):
        """ Plain-text -> Cipher-text
        :param msg: binary_encoded string value
            - Message to be encoded
        :param key: binary_encoded string value
            - Key used
        :returns: binary_encoded string, binary_encoded string
            - <r, F_k(r) ⊗ m>
        """
        from random import getrandbits
        self.__n = len(msg)

        r = binary_encode(getrandbits(self.__n), self.__n)
        cipher = binary_decode(PRF(k=key).generateF(x=r)) ^ binary_decode(msg)
        return r + binary_encode(cipher, self.__n)

    def dec(self, key, c):
        """ Cipher-text -> Plain-text
        :param key: binary_encoded string value
            - Key used while encryption
        :param c: binary_encoded string, binary_encoded string
            - <r, s>, where len(r) == len(key)
        :return: binary_encoded string value
            - Decoded Plaintext
        """
        r_len = len(key)
        dec_msg = binary_decode(
            PRF(k=key).generateF(x=binary_decode(c[:r_len]))) ^ binary_decode(
            c[r_len:])
        return binary_encode(dec_msg, self.__n)


class MAC:
    """ Message Authentication Codes """

    def __init__(self):
        """ n: number of bits in the string"""
        self.__n = None

    def gen(self, n=None):
        """ Key Generator
        :param n: int32/64
            - Number of bits in the string
        :return: binary_encoded string value
        """
        from random import getrandbits
        return binary_encode(getrandbits(n),
                             n) if n is not None else binary_encode(
            getrandbits(self.__n), self.__n)

    def tag(self, msg, key):
        """ Generate tag for a given message
        :param msg: binary_encoded message
            - Message to be digested
        :param key: binary_encoded string value
            - Key used for tagging
        :returns: binary_encoded string value
            - <tag>
            Note: len(tag) == len(key) == len(msg)
        """
        self.__n = len(msg)
        assert (len(key) == len(msg))
        t = PRF(k=key).generateF(x=msg)
        return t

    def verify(self, key, msg, c):
        """ Verify the message digest
         :param key: binary_encoded key value
            - Key for verification of digest
         :param msg: binary_encoded message
            - Message to be digest
         :param c: binary_encoded string value
            - Tagged hash of the message
         :returns: bool
         """
        t = binary_decode(c)
        return t == binary_decode(PRF(k=key).generateF(x=msg))


if __name__ == "__main__":
    msg = "1010010111"
    mac_mech = MAC()
    key = mac_mech.gen(10)

