# Message Authentication Codes

<p style="text-align:right;"> <b> Aman Rojjha </b> <br> 2019111018 </p>

The aim of a message authentication code is to prevent an adversary from
modifying a message sent by one party to another, or from injecting a new
message, without the receiver detecting that the message did not originate from
the intended party. In this setting, we continue to consider the private-key
setting where the communicating parties share a secret key. MACs can be used to:

- ensure integrity for 2 communicating parties.
- ensure integrity for one party communicating with himself over time.

```ad-note
title: Message Authenticating Code MAC
A *message authentication code* consists of **three** probabilistic polynomial-time algorithms $\text{(Gen, Mac, Vrfy)}$ such that:
1. **Gen**:
	- *Input*: $1^n$ (security-parameter)
	- *Output*: $k$ where $|k| \ge n$.
2. **Mac**: the *tag-generation algorithm*
	- *Input*: key $k$, message $m \in \{0,1\}^*$
	- *Output*: tag $t \leftarrow Mac_k(m)$
3. **Vrfy**: the *deterministic verification algorithm*
	- *Input*: key $k$, message $m$, tag $t$
	- *Output*: $b := Vrfy_k(m, t)$, where $b=1$ means *valid* and $b=0$ means invalid.
	
It is required that for every $n$, every key $k$ output by $Gen(1^n)$, and every $m ∈ \{0, 1\}^∗$ , it holds that $Vrfy_k(m, Mac_k(m)) = 1$.
```

## Construction of a fixed-length MAC

```ad-note
title: A fixed-length MAC from any pseudorandom function
Let $F$ be a (*length preserving*) pseudorandom function. Define a fixed-length $MAC$ for messages of length $n$ as follows:
1. **Mac**:
	- *Input*: key $k \in \{0,1\}^n$, message $m \in \{0,1\}^n$
	- *Output*: tag $t := F_k(m)$
2. **Vrfy**:
	- *Input*: key $k \in \{0,1\}^n$, message $m \in \{0,1\}^n$ and tag $t \in \{0,1\}^n$
	- *Output*: 1 if $t == F_k(m)$ else 0
	
```

It is provable that if $F$ is a *pseudorandom function*, the given construction
is a secure fixed-length MAC for messages of length $n$.