# Chosen-Ciphertext Attacks and CCA-Security

<p style="text-align:right;"> <b> Aman Rojjha </b> <br> 2019111018 </p>

Consider a scenario in which a sender encrypts a message $m$ and then transmits
the resulting ciphertext $c$. An attacker who can tamper with the communication
can modify $c$ to generate another ciphertext $c_0$ that is received by the
other party. This receiver will then decrypt $c_0$ to obtain a message $m_0$ .
If $m_0 \ne m$ (and $m_0 \ne ⊥$), this is a violation of integrity.
> This type of attack, in which an adversary causes a receiver to decrypt ciphertexts that the adversary generates, is called a **chosen-ciphertext attack**.

## Construction

```ad-note
title: Encrypt-then-authenticate approach
Let $\Pi_E = (\text{Gen, Enc, Dec})$ be a private-key encryption scheme and let $\Pi_M = (\text{Mac, Vrfy})$ be a  message authentication code, where in each case key generation is done by choosing a uniform $n$-bit key. Define a private-key encryption scheme $\text{(Gen', Enc', Dec')}$ as follows:
1. **Gen'**:
	- *Input*: $1^n$
	- Choose independent, uniform $k_E, k_M \in \{0,1\}^n$
	- *Output*: $(k_E, k_M)$.
2. **Enc'**: 
	- *Input*: $(k_E, k_M)$, plaintext $m$
	- *Output*: $<c, t> := <c \leftarrow Enc_{k_E}(m), t \leftarrow Mac_{k_M}(c)>$.
3. **Dec'**: 
	- *Input*: key $(k_E, k_M)$ and ciphertext $<c, t>$
	- If $\text{Vrfy}_{k_M}(c, t) = =1$, 
		- Output $\text{Dec}_{k_E}(c)$
	- Else output ⊥.
```

- *Strong security* of the $MAC$ ensures that an adversary will be unable to
  generate any valid ciphertext that it did not receive from its encryption
  oracle.
- Also, it effectively renders the decryption oracle useless: for every
  ciphertext $<c, t>$ the adversary submits to its decryption oracle, the
  adversary either already knows the decryption (if it received $<c, t>$ from
  its encryption oracle) or will receive an error.

It is provable that the given scheme is an *authenticated encryption scheme*.