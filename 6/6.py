def binary_encode(value, length):
    """
    :param length: int32
    :param value: int32/64
    :returns binary_value: String (binary format)
    """
    assert (len(bin(value)) - 2 <= length)
    return "0" * (length - len(bin(value)) + 2) + bin(value)[2:]


def binary_decode(binary_value):
    return int(binary_value, 2)


def mod_exp(g, x, p):
    """ g^x mod p"""
    res = 1
    g = g % p
    if g == 0:
        return 0

    while x > 0:
        res = (res * g) % p if x & 1 == 1 else res
        x = x >> 1
        g = (g * g) % p

    return res


def msb(x):
    return x[0]


class PRG:
    """ Pseudo-Random Generator """

    def __init__(self, seed=None, expansion_factor=32):
        """
        Initialize PRG with given/ default values
        :param seed: String s ∈ {0,1}^n
        :param expansion_factor: int
            - L(n) {> n}
        """
        self.__seed = seed if seed is not None else binary_encode(42, 32)
        self.__seed_len = len(self.__seed)
        self.__expansion_factor = expansion_factor if expansion_factor is not None else 64
        # print(self.__seed)
        assert (self.__seed_len >= 3)
        self.__G = prime_dict[
            self.__seed_len - 1] if self.__seed_len <= 64 else int(
            1e9 + 7)  # Prime number not included in the prime_dict
        self.__p = prime_dict[self.__seed_len] if self.__seed_len <= 64 else \
            prime_dict[64]

    def generate(self):
        """ Generate the key from the seed """
        if self.__seed_len >= self.__expansion_factor:
            raise Exception(
                f"L(n) should be greater than n, where n is the seed and L is the output length!")

        # print(f"g: {self.__G}\t p: {self.__p}")
        result = ''
        seed = self.__seed
        for i in range(self.__expansion_factor):
            # print(f"{i} iter:\t bit is: {msb(seed)} \t seed is {seed}")
            result += msb(seed)
            seed = binary_encode(
                mod_exp(self.__G, binary_decode(seed), self.__p),
                self.__seed_len)
        return result


class PRF(PRG):
    """ Pseudo-Random Function """

    def __init__(self, k=None):
        """
        :param k: binary_encoded string value (seed)
        :param x: binary encoded string value (inp)
        """
        super().__init__(seed=k, expansion_factor=2 * len(k))
        self.__x = None
        self.__k = k

    def generateF(self, x=None):
        """
        F_k(x)
        :param x: binary encoded string value (input)
        :return: binary_encoded string value
        """
        assert (len(x) == len(self.__k))
        self.__x = x
        which_half = lambda inp, bit: inp[
                                      :len(inp) // 2] if bit == '0' else inp[
                                                                         len(inp) // 2:]
        seed = self.__k
        for i in self.__x:
            res = self.generate()
            seed = which_half(res, i)
        return seed


class CRHF:
    """ Fixed-length Collision-Resistant Hash Function using the Discrete Logarithm Problem
    - <G, p, g>: <ℤ/nℤ, n, x> where x is a random prime number less than n;
        - G = g^x mod p
    """

    def __init__(self, n=16):
        """
        :param n: int32/64
            - Number of bits in the field order
        """
        self.__order_bits = n
        self.Q = None
        self.G = None

    def gen(self, n):
        """ Generator <G, p, g> where
            - G(x) = g^x mod p
        :param n: int32/64
            - Number of bits in the field order
        :returns key: binary_encoded string value || binary_encoded string value || binary_encoded string value
            - g || q || h
            - 3 * len(order_bits)
        """
        from random import choice
        self.__order_bits = n

        assert (self.__order_bits >= 3)
        self.G = prime_dict[
            self.__order_bits - 1] if self.__order_bits <= 64 else int(
            1e9 + 7)  # Prime number not included in the prime_dict
        self.Q = prime_dict[self.__order_bits] if self.__order_bits <= 64 else \
            prime_dict[64]

        return binary_encode(self.G, self.__order_bits) + binary_encode(self.Q,
                                                                        self.__order_bits) + binary_encode(
            choice(range(self.Q)), self.__order_bits)

    def H(self, key, msg):
        """ H^s (x_1, x_2) = g^{x_1} * h^{x_2}
        :param msg: String
        :param key: g || q || h
        :returns: binary_encoded output value
            - len(output) == self.__order_bits
        """
        assert (len(key) == 3 * self.__order_bits)
        g, q, h = key[:len(key) // 3], key[len(key) // 3: -len(key) // 3], key[
                                                                           :-len(
                                                                               key) / 3]
        x1, x2 = msg
        assert (len(x1) < q and len(x2) < q)
        return binary_encode(mod_exp(g, x1, q) * mod_exp(h, x2, q) % q,
                             self.__order_bits)


if __name__ == "__main__":
    import pickle

    with open('./prime_dict.pickle', 'rb') as handle:
        tmp_prime_dict = pickle.load(handle)

    prime_dict = {3: 7}

    for i, val in tmp_prime_dict.items():
        prime_dict[i - 1] = val
