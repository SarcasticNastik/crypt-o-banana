# Fixed Length Collision-Resistant Hash Function

Hash functions are simply functions that take input of some length $n$ and
compress them into shorter fixed-length outputs.
![[Pasted image 20220308224235.png]]

*Collision* occues when for two given inputs $x_1$ and $x_2$, $h(x_1) = h(x_2)$
for a given hash function $h$.

The following constructions holds owing to computational hardness of the **
Discrete Logarithm Problem**.
![[Pasted image 20220308224411.png]]

The discrete-logarithm assumption is simply the assumption that there exists a
$G$ for which the discrete-logarithm problem is hard.

## Construction

```ad-note
title: A fixed length-hash function
   

Let $P$ be a polynomial time algorithm that on input $1^n$ outputs a cyclic group $G$ of order $q$ (length of $q$ is $n$) and generator $g$.
Thus the group $<G, q, g> := P(1^n)$.
Define a fixed-length hash function $\text{Gen, H}$ as follows:
1. **Gen**:
	- *Input*: $1^n$
	- Obtain $(\mathbb{G}, q, g) := P(1^n)$.
		- Select $h \leftarrow \mathbb{G}$
	- *Output*: $s := <\mathbb{G}, q, g, h>$ as key.
	
2. **H**: 
	- *Input*: key $s=<\mathbb{G}, q, g, h>$ and input $(x_1, x_2) \in \mathbb{Z}_q \times \mathbb{Z}_q$
	- *Output*: $H^s(x_1, x_2) = g^{x_1}h^{x_2}$
```

Under the **DLP** hardness assumption, it is provable that this contruction
gives a *one-way trapdoor function*.