# Merkle-Damgard Transform

It is much easier to construct fixed-length hash functions (i.e., compression
functions) that only accept “short” inputs.

```ad-note
title: The Merkle-Damgard transform
Let $(Gen, h)$ be a compression function for inputs of length $n + n'$ ≥ $2n$ with output length $n$. Fix $\ell ≤ n'$ and $IV ∈ {0, 1}^n$ . Construct hash function $(Gen, H)$ as follows:
- **Gen**: remains unchanged
- **H**: 
	- *Input* - key $s$, string $x \in \{0,1\}^*$ of length $L \lt 2^\ell$
		1. Append a $1$ to $x$, followed by enough zeros so that the length of the resulting string is $\ell$ less than a multiple of $n'$ . Then append $L$, encoded as an $\ell$-bit string. Parse the resulting string as the sequence of $n'$-bit blocks $x_1, . . . , x_B$.
		2. Set $z_0 := IV$.
		3. For $i = 1, \ldots, B$, compute $z_i := h^s(z_{i-1} \| x_i)$.
	- *Output*: $z_B$
```

![[Pasted image 20220308225329.png]]

### Collision Resistance of MD transform

![[Pasted image 20220308225246.png]]

<div style="page-break-after: always;"></div>


![[Pasted image 20220308225317.png]]