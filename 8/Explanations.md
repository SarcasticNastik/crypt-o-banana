# HMAC

Th design of **HMAC**, a message authentication code for arbitrary-length
messages that can be based on any hash function $(Gen_H, H)$ constructed using
the Merkle–Damg˚ard transform applied to a compression function $(Gen_H, h)$.

## Motivation

**MAC** can be extended using *collision-resistant hash functions* as follows.
Say we have a collision-resistant hash function with *input length* $l(n)$ and
fixed-length MAC for $l(n)$-bit communications. Using the MAC to authenticate
the hash of $m$, we can then authenticate arbitrary-length message $m$.

This is safe since MAC prevents the attackers from authenticating any new hash
values and collision-resistance prevents the attackers from finding any new
messages that hash to a previously found messages.

![[Pasted image 20220308225627.png]]

## Construction

```ad-note
title: HMAC
Let $(Gen_H, H)$ be a hash function constructed by applying the Merkle– Damg˚ard transform to a compression function $(Gen_H, h)$ that takes inputs of length $n + n' > 2n + log n + 2$ and generates output of length $n$. Fix distinct constants $opad$, $ipad ∈ \{0, 1\}^{n'}$. Define a MAC as follows:
1. **Gen**: on input $1^n$ , run $Gen_H(1^n)$ to obtain a key $s$. Also choose uniform $k ∈ \{0, 1\}^{n'}$ . Output the key $(s, k)$.
2. **Mac**: on input a key $(s, k)$  and a message $m \in \{0,1\}^*$ output
$$t := H^s((k\otimes opad) \| H^s((k \otimes ipad)\| m))$$
3. **Vrfy**: on input a key $(s, k)$, a message $m ∈ \{0, 1\}^∗$ , and a tag $t$, output 1 if and only $t$ is as defined above.
```

Note that assuming $G^s$ is a pseudorandom generator, $\tilde\Pi$ is a secure
fixed-length MAC for messages of length $n'$ , and $(Gen_H, h)$ is collision
resistant. Then HMAC is a secure MAC (for arbitrary-length messages).

## Why `ipad` and `opad`?

For the *hash-and-MAC* approach all that is required is for the inner
computation to be **collision resistant**, which does not require any secret
key. The reason for including a secret key as part of the inner computation is
that this allows security of HMAC to be based on the assumption that $(Gen_H, H)
$ is weakly collision resistant, where (informally) this refers to an experiment
in which an attacker needs to find collisions in a secretly keyed hash function.
This is a weaker condition than collision resistance, and hence is potentially
easier to satisfy. Ideally, independent keys $k_{in}$, $k_{out}$ should have
been used in the inner and outer computations. To reduce the key length of HMAC,
a single key $k$ is used to derive the values using ipad and opad. Actully we
only required ipad and opad to differ at atleast one bit for the above reasons,
but we choose them different just to be safe.