# Crypt-O-banana

> Code for cryptographic algorithms and proofs completed during S'22 for the course `Principles in Information Security` .



## Directory Structure

```zsh
.
├── 1 													- PRG
│  ├── 1.py
│  ├── Explanation.md
│  ├── Explanation.pdf
│  └── fig1.png
├── 2													- PRF
│  ├── 2.py
│  ├── Explanation.md
│  ├── Explanation.pdf
│  └── fig.png
├── 3													- Stream-Ciphers and CPA
│  ├── 3.py
│  ├── Explanation.md
│  └── Explanation.pdf
├── 4													- Fixed-length MAC
│  ├── 4.py
│  ├── Explanation.md
│  └── Explanation.pdf
├── 5													- CCA attacks and Encrypt-then-authenticate approach
│  ├── 5.py
│  ├── Explanation.md
│  └── Explanation.pdf
├── 6													- Fixed-length Collision resistant hash function
│  ├── 6.py
│  ├── Explanations.md
│  └── Explanations.pdf
├── 7													- Merkle-Damgard Transform
│  ├── 7.py
│  ├── Explanations.md	
│  └── Explanations.pdf
├── 8													- HMAC, arbitrary-length MAC
│  ├── 8.py
│  ├── Explanations.md
│  └── Explanations.pdf
├── __init__.py
├── helpers.py	
├── prime_dict.pickle
```

