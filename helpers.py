def binary_encode(value, length):
    """
    :param length: int32
    :param value: int32/64
    :returns binary_value: String (binary format)
    """
    assert (len(bin(value)) - 2 <= length)
    return "0" * (length - len(bin(value)) + 2) + bin(value)[2:]

def binary_decode(binary_value):
    return int(binary_value, 2)

def mod_exp(g, x, p):
    """ g^x mod p"""
    res = 1
    g = g % p
    if g == 0:
        return 0

    while x > 0:
        res = (res * g) % p if x & 1 == 1 else res
        x = x >> 1
        g = (g * g) % p

    return res

def msb(x):
    return x[0]

import pickle
with open('./prime_dict.pickle', 'rb') as handle:
    tmp_prime_dict = pickle.load(handle)

prime_dict = {3: 7}

for i, val in tmp_prime_dict.items():
    prime_dict[i-1] = val
